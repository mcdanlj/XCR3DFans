I have an XCR3D hotend which I like, but it comes with a terrible
30mm fan; it was loud to start with and then started eating its
bearings after a few hours.

This adapter lets you use a 40mm fan, which is quieter, attached
to the existing metal bracket. It also allows you to attach a 40mm
radial (centrifugal) part cooling fan to either side or both sides.
I have not added a duct to move the cooling air closer to the
nozzle because it hasn't yet been necessary.  Maybe later I will
make one that can glue into the opening in the fan bracket.

I started out trying to make this design parametric and then gave up.
If you change the thickness of the adapter it might work, but watch
the location of the cooling fan bracket holes.  I have tested it
only as provided, and using only the left fan bracket.

You can comment out `print_set();` and uncomment `assembly();` in
OpenSCAD to see what the assembly is intended to look like.

I printed in PLA+ so far.  I may re-do in ABS later.

## Hardware Required

* M3x0.5 tap, spiral flute recommended.

* 2.5mm and 3mm drill bits optional depending on quality of print.

* 2x M3x5 or M3x6 hex socket cap screws (up to M3x10 should
  work) to attach the lower part of the fan adapter to the
  existing metal bracket.

* 2x M3x5 button head hex socket cap screws to attach the
  upper part of the fan adapter to the existing metal bracket.
  Hex socket cap screws will work too, but I used button head
  screws to stay out of the airflow as much as possible.

* The M3 screws previously used to attach the 30mm fan to the
  bracket are re-used to attach the 40mm fan to the adapter

* 2x M3x5 button head hex socket cap screws for *each* side
  that you want to attach a part cooling fan.

* 3x M1.5x6, M1.5x8, M1.7x6, or M1.7x8 self-tapping screws
  for *each* side that you want to attach a part cooling fan.

## Instructions

Print the parts you plan to use.

If you need to clean holes, use a 2.5mm drill bit for 3mm threaded
holes for attaching 40mm fan (the outer holes in the adapter) and
only the side holes that you intend to use to attach a part cooling
fan. Use a 3mm drill bit to clean the holes for mounting to the
bracket (the inner set of holes on the adapter).  Do not clean the
small holes in the cooling fan bracket.

Use an M3x0.5 spiral flute tap for threads for attaching the 40mm
fan and the side holes you will use to attach the cooling fan
bracket.

Remove the bracket from the hot end using the set screws on the
top of the bracket.  Remove the fan from the bracket, set aside
the supplied screws.

Use 2 M3xx5/6 hex socket screws to attach the bottom of the adapter
to the bracket. Use 2 M3x5 button head screws to attach the top
of the adapter to the bracket. Tighten them firmly.

Using the original fan mounting screws, attach the 40mm fan to
the adapter.

Using 2 M3x5 button head screws, loosely attach the cooling fan
adapter bracket(s) you intend to use.

Re-attach the bracket to the hot end.

Z-home the bed. Put a 1-2mm shim under the cooling fan bracket
bracket(s) and tighten the M3x5 button head screws.

Use three small self-tapping screws to attach the cooling fan(s)
to the cooling fan bracket(s). Note that the upper forward screw
hole on the cooling fan is not used; a button head screw is in
the way. You will probably have to hold the bracket while
tightening the screws; it is fragile.

As usual, no warranty, it works for me, your milage may vary,
good luck but if it breaks you get to keep both pieces and any
melted plastic or bad prints that result.

Copyright Michael K Johnson, CC0 License
https://creativecommons.org/share-your-work/public-domain/cc0/
