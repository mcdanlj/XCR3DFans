//Copyright Michael K Johnson, CC0 License
//https://creativecommons.org/share-your-work/public-domain/cc0/

pitch30 = 24;
d30 = 27.5;

pitch40 = 32;
d40 = 38;

pitch_radial = 35;

adapter_height = 8;
shell=2;

$fn=120;

module airpath(h=adapter_height) {
    multmatrix([ [ 1  , 0  , 0  , 0   ],
                 [ 0  , 1  , 0.5*(d40-d30)/h, 0   ],
                 [ 0  , 0  , 1  , 0   ],
                 [ 0  , 0  , 0  , 1   ] ]) cylinder(d1=d30, d2=d40, h=h);
}

module adapter(h=adapter_height) {
    difference() {
        translate([0, 5, 0]) hull() for (x=[-1, 1], y=[-1, 1]) {
            translate([x*pitch40/2, y*pitch40/2, 0]) cylinder(d=40-pitch40+2, h=adapter_height);
        }
        translate([0, 0, -0.01]) airpath(h=h+0.02);
        for (x=[-1, 1], y=[-1, 1]) {
            // 30mm fan mount holes
            translate([x*pitch30/2, y*pitch30/2, 0]) {
                cylinder(d=3.25, h=adapter_height*3, center=true);
                translate([0, 0, shell]) cylinder(d=6, h=adapter_height, center=false);
            }
            // 40mm fan mount screw holes
            translate([x*pitch40/2, 5+ y*pitch40/2, shell]) {
                cylinder(d=2.6, h=adapter_height, center=false);
            }
            // screw holes for print cooling fan mount
            translate([x*21.01, 5 - y*10, h/2]) rotate([0, -x*90, 0]) cylinder(d=2.6, h=4.5);
        }
        // cut off the top and bottom (anti z-fighting)
        translate([0, 0, -1]) cylinder(d=50, h=1);
        translate([0, 0, h]) cylinder(d=50, h=1);
    }
}
// adapter();

module cooling_mount() {
    mount_x = 0;
    mount_y = 5;
    duct_x = 30;
    duct_y = 10;
    t = shell*2; // main body thickness, leaving space for mounting screw heads
    difference() {
        union() {
            hull() {
                // main body
                for (x=[-1, 1], y=[-1, 1]) {
                    translate([x*pitch_radial/2, y*pitch_radial/2, 0]) cylinder(d=(40-pitch_radial), h=t);
                }
                // duct
                translate([0, -(20+duct_y/2), shell]) cube([duct_x, duct_y, t], center=true);
            }
            hull() {
                // attachment tang
                for (y=[-1, 1]) {
                    translate([-(mount_x + pitch_radial/2), mount_y+y*pitch_radial/2, 0]) cylinder(d=(6), h=shell);
                }
            }
        }
        for (x=[-1, 1], y=[-1, 1]) {
            // fan screw holes
            translate([x*pitch_radial/2, y*pitch_radial/2, 0]) cylinder(d=1.4, h=t*4, center=true);
        }
        for (y=[-1, 1]) {
            // mounting screw slots
            translate([0, y*10, 0]) {
                // through holes
                hull() {
                    for (i=[-1, 1])
                    translate([-pitch_radial/2, mount_y + i*6, 0]) cylinder(d=3.25, h=t*4, center=true);
                }
                // space for heads (m3x5 or m3x6 button-head)
                hull() {
                    for (i=[-1, 1])
                    translate([-pitch_radial/2, mount_y + i*6, shell]) cylinder(d=6, h=t, center=false);
                }
            }
        }
        // duct
        translate([0, -(20+duct_y/2-shell/2), shell]) cube([duct_x-t, duct_y-shell, t*1.01], center=true);
        // lightness
        cylinder(d=28, h=t*3, center=true);
    }
    // duct curve
    difference() {
        translate([0, -20, t]) rotate([0, 90, 0]) cylinder(r=duct_y, h=duct_x, center=true);
        translate([0, -20, t]) rotate([0, 90, 0]) cylinder(r=duct_y-shell, h=duct_x-t, center=true);
        translate([0, -20, t-20]) cube([40, 40, 40], center=true);
        translate([0, 0, t+20]) cube([40, 40, 40], center=true);
    }

}
//cooling_mount();

module assembly() {
    adapter();
    translate([21, 0, -(11.5 + shell)]) rotate([0, 90, 0]) cooling_mount();
    mirror([1, 0, 0]) translate([21, 0, -(11.5 + shell)]) rotate([0, 90, 0]) cooling_mount();
}
//assembly();

module print_set() {
    translate([0, 20, 0]) adapter();
    translate([-25, -25, 0]) cooling_mount();
    mirror([1, 0, 0]) translate([-25, -25, 0]) cooling_mount();
}
print_set();